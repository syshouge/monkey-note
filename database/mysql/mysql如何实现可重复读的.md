一个事务要更新一行，如果刚好有另外一个事务拥有这一行的行锁，会被锁住，进入等待状态。既然进入了等待状态，那么等到这个事务自己获取到行锁要更新数据的时候，它读到的值又是什么呢?

```sql
CREATE TABLE `t` ( `id` int(11) NOT NULL, `k` int(11) DEFAULT NULL, PRIMARY KEY (`id`)
) ENGINE=InnoDB;
insert into t(id, k) values(1,1),(2,2);
```

| 事务A                                     | 事务B                                                        | **事务**C                        |
| ----------------------------------------- | ------------------------------------------------------------ | -------------------------------- |
| start transaction consistent snapashot;   |                                                              |                                  |
|                                           | start transaction consistent snapashot;                      |                                  |
|                                           |                                                              | update t set k=k+1 where id = 1; |
|                                           | update t set k=k+1 where id = 1;<br />select * from  where id = 1; |                                  |
| select * from  where id = 1;<br />commit; |                                                              |                                  |
|                                           | commit;                                                      |                                  |

> 事务C没有显式地使用begin/commit，表示这个update语句本身就是一个事务，语句完成的时候会自动提交。

可重复读隔离级别下，事务在启动的时候就“拍了个整个库的快照”。如果一个库有100G，那么我启动一个事务，MySQL就要拷⻉100G的数据出来，这个过程得多慢啊。但是平时事务执行起来却是非常快的。不是全部拷贝出来那是怎么实现的呢? 

InnoDB里面每个事务有一个唯一的事务ID，叫作transaction id。它是在事务开始的时候向InnoDB的事务系统申请的，是按申请顺序严格递增的。

而每行数据也都是有多个版本的。每次事务更新数据的时候，都会生成一个新的数据版本，并且把transaction id赋值给这个数 据版本的事务ID，记为row trx_id。同时，旧的数据版本要保留，并且在新的数据版本中，能够有信息可以直接拿到它。

数据表中的一行记录，其实可能有多个版本(row)，每个版本有自己的row trx_id。
![image.png](https://upload-images.jianshu.io/upload_images/1449291-6d4be340a78beefa.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


图中虚线框里是同一行数据的4个版本，当前最新版本是V4，k的值是22，它是被transaction id 为25的事务更新的，因此它的row trx_id也是25。语句更新会生成undo log(回滚日志)，图中的三个虚线箭头，就是undo log。

按照可重复读的定义，一个事务启动的时候，能够看到所有已经提交的事务结果。但是之后，这个事务执行期间，其他事务的更新对它不可⻅。

一个事务只需要在启动的时候声明说，“以我启动的时刻为准，如果一个数据版本是在我启动之前生成的，就认;如果是我启动以后才生成的，我就不认，我必须要找到它的上一个版本”。

如果“上一个版本”也不可⻅，那就得继续往前找。如果是这个事务自己更新的数据，它自己还是要认的。

在实现上， InnoDB为每个事务构造了一个数组，用来保存这个事务启动瞬间，当前正在“活跃”的所有事务ID。“活跃”指的就 是，启动了但还没提交。数组里面事务ID的最小值记为低水位，当前系统里面已经创建过的事务ID的最大值加1记为高水位。 这个视图数组和高水位，就组成了当前事务的一致性视图(read-view)。而数据版本的可⻅性规则，就是基于数据的row trx_id和这个一致性视图的对比结果得到的。

**InnoDB利用了“所有数据都有多个版本”的这个特性，实现了“秒级创建快照”的能力。**

回到我们最开始的表格，看看最后执行的结果是多少。做如下假设:

1. 事务A开始前，系统里面只有一个活跃事务ID是99;
2. 事务A、B、C的版本号分别是100、101、102，且当前系统里只有这四个事务; 
3.  三个事务开始前，(1,1)这一行数据的row trx_id是90。

事务A的视图数组就是[99,100], 事务B的视图数组是[99,100,101], 事务C的视图数组是[99,100,101,102]。为了简化分析，我先把其他干扰语句去掉，只画出跟事务A查询逻辑有关的操作:
![image.png](https://upload-images.jianshu.io/upload_images/1449291-04c52fc95d617b94.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

第一个有效更新是事务C，把数据从(1,1)改成了(1,2)。这时候，这个数据的最新版本的row trx_id是102，而90这个版本已经成为了历史版本。 第二个有效更新是事务B，把数据从(1,2)改成了(1,3)。这时候，这个数据的最新版本(即row trx_id)是101，而102又成为了历史版本。

***事务B的update语句，如果按照一致性读，好像结果不对哦?***

事务B的视图数组是先生成的，之后事务C才提交，不是应该看不⻅(1,2)吗，怎么能算出(1,3)来?
![image.png](https://upload-images.jianshu.io/upload_images/1449291-8d8632394d668b27.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


事务B在更新之前查询一次数据，这个查询返回的k的值确实是1。 但是，当它要去更新数据的时候，就不能再在历史版本上更新了，否则事务C的更新就丢失了。因此，事务B此时的set k=k+1是在(1,2)的基础上进行的操作。 所以，这里就用到了这样一条规则:更新数据都是先读后写的，而这个读，只能读当前的值，称为**“当前读”**(**current read**)。

在更新的时候，当前读拿到的数据是(1,2)，更新后生成了新版本的数据(1,3)，这个新版本的row trx_id是101。

所以，在执行事务B查询语句的时候，一看自己的版本号是101，最新数据的版本号也是101，是自己的更新，可以直接使用， 所以查询得到的k的值是3。

select语句如果加锁，也是当前读。

如果把事务A的查询语句select * from t where id=1修改一下，加上lock in share mode 或 for update，也都可以读到版本号是101的数据，返回的k的值是3。下面这两个select语句，就是分别加了读锁(S锁，共享锁)和写锁(X锁，排他锁)。

```mysql
select k from t where id=1 lock in share mode; 
select k from t where id=1 for update;
```
| 事务A                                     | 事务B                                                        | **事务**C`                                                   |
| ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| start transaction consistent snapashot;   |                                                              |                                                              |
|                                           | start transaction consistent snapashot;                      |                                                              |
|                                           |                                                              | start transaction consistent snapashot;<br />update t set k=k+1 where id = 1; |
|                                           | update t set k=k+1 where id = 1;<br />select * from  where id = 1; |                                                              |
| select * from  where id = 1;<br />commit; |                                                              | commit;                                                      |
|                                           | commit;                                                      |                                                              |

事务C’的不同是，更新后并没有⻢上提交，在它提交前，事务B的更新语句先发起了。前面说过了，虽然事务C’还没提交，但是(1,2)这个版本也已经生成了，并且是当前的最新版本。那么，事务B的更新语句会怎么处理呢?
![image.png](https://upload-images.jianshu.io/upload_images/1449291-d0251392ff6da53a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

两阶段锁协议，事务C’没提交，也就是说(1,2)这个版本上的写锁还没释放。 而事务B是当前读，必须要读最新版本，而且必须加锁，因此就被锁住了，必须等到事务C’释放这个锁，才能继续它的当前读。

回到最初的问题，事务的可重复读的能力是怎么实现的?

>  可重复读的核心就是一致性读(consistent read);而事务更新数据的时候，只能用当前读。如果当前的记录的行锁被其他事务占用的话，就需要进入锁等待。

