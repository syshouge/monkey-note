我们都知道一张表可以建很多索引，我们在执行一次查询时并没有执行使用哪个索引，是谁在帮助我们决定使用什么索引？是mysql优化器在判断该使用什么索引，优化器判断的有的时候未必是正确的。

### 优化器的逻辑

优化器选择索引的目的，是找到一个最优的执行方案，并用最小的代价去执行语句。所谓小的代价衡包括`扫描的行数`、是否使用临时表、是否排序等因素。

#### 扫描行数是怎么判断的?

MySQL在真正开始执行语句之前，并不能精确地知道满足这个条件的记录有多少条，而只能根据统计信息来估算记录数。这个统计信息就是索引的“区分度”。显然，一个索引上不同的值越多，这个索引的区分度就越好。而一个索引上不同的值的个 数，我们称之为“基数”(**cardinality**)。也就是说，这个基数越大，索引的区分度越好。比如问在现在有张表，有性别字段，在性别上建索引行不行？行当然是行的，只是这个索引建的意义不是很大，这个索引基数太小，区分度不大。我们可以使用show index方法，看到一个索引的基数。我们可以使用show index方法，看到一个索引的基数。

**MySQL是怎样得到索引的基数的呢?**

为什么要采样统计呢?因为把整张表取出来一行行统计，虽然可以得到精确的结果，但是代价太高了，所以只能选择“采样统计”。

采样统计的时候，InnoDB默认会选择N个数据⻚，统计这些⻚面上的不同值，得到一个平均值，然后乘以这个索引的⻚面数， 就得到了这个索引的基数。

而数据表是会持续更新的，索引统计信息也不会固定不变。所以，当变更的数据行数超过1/M的时候，会自动触发重新做一次 索引统计。

在MySQL中，有两种存储索引统计的方式，可以通过设置参数innodb_stats_persistent的值来选择:

* 设置为on的时候，表示统计信息会持久化存储。这时，默认的N是20，M是10。

* 设置为off的时候，表示统计信息只存储在内存中。这时，默认的N是8，M是16。

没有场景的讨论都是耍流氓，我们就来假设一种场景，先建一张表，表里有a，b两个字段，并分别建立索引：

```sql
CREATE TABLE `t` (
`id` int(11) NOT NULL, `a` int(11) DEFAULT NULL, `b` int(11) DEFAULT NULL, PRIMARY KEY (`id`),
KEY `a` (`a`),
KEY `b` (`b`)
) ENGINE=InnoDB;
```

我们往表t中插入10万行记录，取值按整数递增，即:(1,1,1)，(2,2,2)，(3,3,3) 直到(100000,100000,100000)。

```sql
delimiter ;;
create procedure idata() begin
declare i int;
set i=1; while(i<=100000)do
insert into t values(i, i, i);
    set i=i+1;
  end while;
end;;
delimiter ;
call idata();
```

我们先来分析第一条sql

```
select * from t where a between 10000 and 20000;
```

就这？就这还用分析？a上有索引，明显使用索引a的。
![image.png](https://upload-images.jianshu.io/upload_images/1449291-4063405c52dbd492.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


这条查询语句的执行也确实符合预期，key这个字段值是’a’，表示优化器选择了索引a。我们使用复杂点的场景

| session A                                   | session B                                                |
| ------------------------------------------- | -------------------------------------------------------- |
| start transaction with consistent snapshot; |                                                          |
|                                             | delete from t;<br/>call idata();                         |
|                                             | explain select * from t where a between 10000 and 20000; |
| commit                                      |                                                          |

session A的操作你已经很熟悉了，它就是开启了一个事务。随后，session B把数据都删除后，又调用了 idata这个存 储过程，插入了10万行数据。

这时候，session B的查询语句select * from t where a between 10000 and 20000就不会再选择索引a了。我们可以通过慢查 询日志(slow log)来查看一下具体的执行情况。

为了说明优化器选择的结果是否正确，我增加了一个对照，即:使用force index(a)来让优化器强制使用索引a(这部分内容， 我还会在这篇文章的后半部分中提到)。

下面的三条SQL语句，就是这个实验过程。

```sql
set long_query_time=0;
select * from t where a between 10000 and 20000; /*Q1*/
select * from t force index(a) where a between 10000 and 20000 /*Q2*/
```

* 第一句，是将慢查询日志的阈值设置为0，表示这个线程接下来的语句都会被记录入慢查询日志中;
*  第二句，Q1是session B原来的查询;
* 第三句，Q2是加了force index(a)来和session B原来的查询语句执行情况对比。

![image.png](https://upload-images.jianshu.io/upload_images/1449291-4a2a80e270ada236.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


rows这个字段表示的是预计扫描行数。

其中，Q1的结果还是符合预期的，rows的值是104620;但是Q2的rows值是37116，偏差就大了。而第一张图我们用explain命令

rows这个字段表示的是预计扫描行数。 看到的rows是只有10001行，是这个偏差误导了优化器的判断。

到这里，可能你的第一个疑问不是为什么不准，而是优化器为什么放着扫描37000行的执行计划不用，却选择了扫描行数是 100000的执行计划呢?

这是因为，如果使用索引a，每次从索引a上拿到一个值，都要回到主键索引上查出整行数据，这个代价优化器也要算进去 的。

而如果选择扫描10万行，是直接在主键索引上扫描的，没有额外的代价。 优化器会估算这两个选择的代价，从结果看来，优化器认为直接扫描主键索引更快。当然，从执行时间看来，这个选择并不是最优的。

**MySQL选错索引，这件事儿还得归咎到没能准确地判断出扫描行数。**

既然是统计信息不对，那就修正。analyze table t 命令，可以用来重新统计索引信息。如果只是索引统计不准确，通过analyze命令可以解决很多问题，但是前面我们说了，优化器可不止是看扫描行数。依然是基于这个表t，我们看看另外一个语句:

```sql
select * from t where (a between 1 and 1000) and (b between 50000 and 100000) order by b limit 1;
```

为了便于分析，我们先来看一下a、b这两个索引的结构图。

![image.png](https://upload-images.jianshu.io/upload_images/1449291-5513b16e30115567.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


使用索引a进行查询，那么就是扫描索引a的前1000个值，然后取到对应的id，再到主键索引上去查出每一行，然后根据

字段b来过滤。显然这样需要扫描1000行。

如果使用索引b进行查询，那么就是扫描索引b的最后50001个值，与上面的执行过程相同，也是需要回到主键索引上取值再判

断，所以需要扫描50001行。

```sql
explain select * from t where (a between 1 and 1000) and (b between 50000 and 100000) order by b limit 1
```

可以看到，返回结果中key字段显示，这次优化器选择了索引b，而rows字段显示需要扫描的行数是50198。

从这个结果中，你可以得到两个结论:

1. 扫描行数的估计值依然不准确;
2. 这个例子里MySQL又选错了索引。

#### 索引选择异常和处理

其实大多数时候优化器都能找到正确的索引，但偶尔你还是会碰到我们上面举例的这两种情况:

1. 一种方法是，像我们第一个例子一样，采用**force index**强行选择一个索引。
2. 第二种方法就是，我们可以考虑修改语句，引导**MySQL**使用我们期望的索引。比如，在这个例子里，显然把“order by b limit 1” 改成 “order by b,a limit 1” ，语义的逻辑是相同的。
3. 第三种方法是，在有些场景下，我们可以新建一个更合适的索引，来提供给优化器做选择，或删掉误用的索引。