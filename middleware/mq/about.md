一、为什么使用消息队列？
1、解耦
2、异步
3、削峰

二、使用了消息队列会有什么缺点?
1、系统可用性降低。任何增量性的解决方式，在带来方便的同时也会带来新的问题，如果mq系统挂了，订阅者都无法进行正常消费
2、系统复杂性增加。要考虑消息一致性问题、如何保证消息不被重复消费，如何保证保证消息可靠传输。

三、选型
特性	    ActiveMQ	        RabbitMQ	        RocketMQ	            kafka
开发语言	java	           erlang	            java	                scala
单机吞吐量	万级	            万级	                10万级	                10万级
时效性     ms级	              us级	                ms级	                    ms级以内
可用性	    高(主从架构)	        高(主从架构)	        非常高(分布式架构)          非常高(分布式架构)

功能特性	成熟的产品，       基于erlang开发，        MQ功能比较完             只支持主要的MQ功能
        在很多公司得到应用；   所以并发能力很        扩展性佳                    像一些消息查询
        有较多的文档；        延时很低                                         消息回溯等功能没有提供
        各种协议支持较好	管理界面较丰富 		                                是为大数据准备的

四、如何保障消息队列的高可用
以rcoketMQ为例，他的集群就有多master 模式、多master多slave异步复制模式、多 master多slave同步双写模式。
Producer 与 NameServer集群中的其中一个节点（随机选择）建立长连接，定期从 NameServer 获取 Topic 路由信息，并向提供 Topic 服务的 Broker Master 建立长连接，且定时向 Broker 发送心跳。Producer 只能将消息发送到 Broker master，但是 Consumer 则不一样，它同时和提供 Topic 服务的 Master 和 Slave建立长连接，既可以从 Broker Master 订阅消息，也可以从 Broker Slave 订阅消息。
以kafka为例，一个典型的Kafka集群中包含若干Producer（可以是web前端产生的Page View，或者是服务器日志，系统CPU、Memory等），若干broker（Kafka支持水平扩展，一般broker数量越多，集群吞吐率越高），若干Consumer Group，以及一个Zookeeper集群。Kafka通过Zookeeper管理集群配置，选举leader，以及在Consumer Group发生变化时进行rebalance。Producer使用push模式将消息发布到broker，Consumer使用pull模式从broker订阅并消费消息。

五、如何保证消息不被重复消费？
其实无论是那种消息队列，造成重复消费原因其实都是类似的。正常情况下，消费者在消费消息时候，消费完毕后，会发送一个确认信息给消息队列，消息队列就知道该消息被消费了，就会将该消息从消息队列中删除。只是不同的消息队列发送的确认信息形式不同,例如RabbitMQ是发送一个ACK确认消息，RocketMQ是返回一个CONSUME_SUCCESS成功标志，kafka实际上有个offset的概念，简单说一下(如果还不懂，出门找一个kafka入门到精通教程),就是每一个消息都有一个offset，kafka消费过消息后，需要提交offset，让消息队列知道自己已经消费过了。那造成重复消费的原因?，就是因为网络传输等等故障，确认信息没有传送到消息队列，导致消息队列不知道自己已经消费过该消息了，再次将该消息分发给其他的消费者。
1、比如，你拿到这个消息做数据库的insert操作。那就容易了，给这个消息做一个唯一主键，那么就算出现重复消费的情况，就会导致主键冲突，避免数据库出现脏数据。
2、再比如，你拿到这个消息做redis的set的操作，那就容易了，不用解决，因为你无论set几次结果都是一样的，set操作本来就算幂等操作。
3、如果上面两种情况还不行，上大招。准备一个第三方介质,来做消费记录。以redis为例，给消息分配一个全局id，只要消费过该消息，将<id,message>以K-V形式写入redis。那消费者开始消费前，先去redis中查询有没消费记录即可。

六、如何保证消费的可靠性传输?
其实这个可靠性传输，每种MQ都要从三个角度来分析，生产者弄丢数据、消息队列弄丢数据、消费者弄丢数据
生产者:RocketMQ分为同步刷盘和异步刷盘两种方式，默认的是异步刷盘，就有可能导致消息还未刷到硬盘上就丢失了，可以通过设置为同步刷盘的方式来保证消息可靠性，这样即使MQ挂了，恢复的时候也可以从磁盘中去恢复消息。
消费者:RocketMQ默认是需要消费者回复ack确认，消费方不返回ack确认，重发的机制根据MQ类型的不同发送时间间隔、次数都不尽相同，如果重试超过次数之后会进入死信队列，需要手工来处理了。（Kafka没有这些）

七、如何保证消息的顺序性？
通过某种算法，将需要保持先后顺序的消息放到同一个消息队列中(kafka中就是partition,rabbitMq中就是queue)。然后只用一个消费者去消费该队列。
如果为了吞吐量，有多个消费者去消费怎么办？生产者保障出队前的顺序，出队后需要消费者来保障。

八、导致消息积压怎么处理？
1、消费者出错，肯定是程序或者其他问题导致的，如果容易修复，先把问题修复，让consumer恢复正常消费
2、如果时间来不及处理很麻烦，做转发处理，写一个临时的consumer消费方案，先把消息消费，然后再转发到一个新的topic和MQ资源，这个新的topic的机器资源单独申请，要能承载住当前积压的消息
3、处理完积压数据后，修复consumer，去消费新的MQ和现有的MQ数据，新MQ消费完成后恢复原状

九、RocketMQ实现原理
RocketMQ由NameServer注册中心集群、Producer生产者集群、Consumer消费者集群和若干Broker（RocketMQ进程）组成，它的架构原理是这样的：
1、Broker在启动的时候去向所有的NameServer注册，并保持长连接，每30s发送一次心跳
2、Producer在发送消息的时候从NameServer获取Broker服务器地址，根据负载均衡算法选择一台服务器来发送消息
3、Conusmer消费消息的时候同样从NameServer获取Broker地址，然后主动拉取消息来消费

十、RocketMQ的Broker是怎么保存数据的呢？
RocketMQ主要的存储文件包括commitlog文件、consumequeue文件、indexfile文件。
Broker在收到消息之后，会把消息保存到commitlog的文件当中，而同时在分布式的存储当中，每个broker都会保存一部分topic的数据，同时，每个topic对应的messagequeue下都会生成consumequeue文件用于保存commitlog的物理位置偏移量offset，indexfile中会保存key和offset的对应关系。
CommitLog文件保存于${Rocket_Home}/store/commitlog目录中，从图中我们可以明显看出来文件名的偏移量，每个文件默认1G，写满后自动生成一个新的文件。
由于同一个topic的消息并不是连续的存储在commitlog中，消费者如果直接从commitlog获取消息效率非常低，所以通过consumequeue保存commitlog中消息的偏移量的物理地址，这样消费者在消费的时候先从consumequeue中根据偏移量定位到具体的commitlog物理文件，然后根据一定的规则（offset和文件大小取模）在commitlog中快速定位。

十一、RocketMQ为什么速度快？
是因为使用了顺序存储、Page Cache和异步刷盘。
1、我们在写入commitlog的时候是顺序写入的，这样比随机写入的性能就会提高很多
2、写入commitlog的时候并不是直接写入磁盘，而是先写入操作系统的PageCache
3、最后由操作系统异步将缓存中的数据刷到磁盘

十二、RocketMQ组成部分有哪些？
Nameserver-无状态，动态列表；这也是和zookeeper的重要区别之一。zookeeper是有状态的。
Producer-消息生产者，负责发消息到Broker。
Broker-就是MQ本身，负责收发消息、持久化消息等。
Consumer-消息消费者，负责从Broker上拉取消息进行消费，消费完进行ack。

十三、RocketMQ消费模式有几种？
1、集群消费
一条消息只会被同Group中的一个Consumer消费
多个Group同时消费一个Topic时，每个Group都会有一个Consumer消费到数据
2、广播消费
消息将对一个Consumer Group 下的各个 Consumer 实例都消费一遍。即即使这些 Consumer 属于同一个Consumer Group ，消息也会被 Consumer Group 中的每个 Consumer 都消费一次。

十四、RocketMQ如何保证消息不丢失？
Producer端
采取send()同步发消息，发送结果是同步感知的。
发送失败后可以重试，设置重试次数。默认3次。
Broker端
修改刷盘策略为同步刷盘。默认情况下是异步刷盘的。
# master 节点配置
flushDiskType = SYNC_FLUSH
brokerRole=SYNC_MASTER
# slave 节点配置
brokerRole=slave
flushDiskType = SYNC_FLUSH
集群部署
Consumer端
完全消费正常后在进行手动ack确认

十五、rocketmq角色
1、Broker
理解成RocketMQ本身
broker主要用于producer和consumer接收和发送消息
broker会定时向nameserver提交自己的信息
是消息中间件的消息存储、转发服务器
每个Broker节点，在启动时，都会遍历NameServer列表，与每个NameServer建立长连接，注册自己的信息，之后定时上报
2、Nameserver
理解成zookeeper的效果，只是他没用zk，而是自己写了个nameserver来替代zk
底层由netty实现，提供了路由管理、服务注册、服务发现的功能，是一个无状态节点
nameserver是服务发现者，集群中各个角色（producer、broker、consumer等）都需要定时向nameserver上报自己的状态，以便互相发现彼此，超时不上报的话，nameserver会把它从列表中剔除
nameserver可以部署多个，当多个nameserver存在的时候，其他角色同时向他们上报信息，以保证高可用，
NameServer集群间互不通信，没有主备的概念
nameserver内存式存储，nameserver中的broker、topic等信息默认不会持久化，所以他是无状态节点
3、Producer
消息的生产者
随机选择其中一个NameServer节点建立长连接，获得Topic路由信息（包括topic下的queue，这些queue分布在哪些broker上等等）
接下来向提供topic服务的master建立长连接（因为rocketmq只有master才能写消息），且定时向master发送心跳
4、Consumer
消息的消费者
通过NameServer集群获得Topic的路由信息，连接到对应的Broker上消费消息
由于Master和Slave都可以读取消息，因此Consumer会与Master和Slave都建立连接进行消费消息
5、核心流程
Broker都注册到Nameserver上
Producer发消息的时候会从Nameserver上获取发消息的topic信息
Producer向提供服务的所有master建立长连接，且定时向master发送心跳
Consumer通过NameServer集群获得Topic的路由信息
Consumer会与所有的Master和所有的Slave都建立连接进行监听新消息

