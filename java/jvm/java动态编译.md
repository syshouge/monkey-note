## 1、概念理解

我们都知道java语言是需要编译成class文件加载到jvm才能执行的，一般性的需求都是开发人员开发好java代码再通过maven进行编译打包最终部署项目。然而有一种需求，需要在项目运行期间“开发”，打个比喻，传统的开发就是自己录制好视频再发布到网上，而动态编译就像是现场直播，现场开发现场编译现场运行，是不是有点像人生，一生都是现场直播。

## 2、需求背景
面对源源不断的各类需求，加之不同的需求可能使用的还是分散在各个不同服务接口，那么一般性的针对业务去开发的解决方式显然不能满足需求的快速更新迭代，那么有没有一种方式来放飞传统针对每一个业务去每次开发新的业务代码呢？有没有一种方式让开发不再重复性的Code呢？编排引擎很显然是一种新方式，针对已有的服务接口可以进行自由编织新业务的入参和结果，配合流程引擎（另一个引擎）嵌入服务接口，不再重复性写很多雷同的业务，并且编排引擎支持版本的迭代，可以很好的应对不同业务的变更。

## 3、流程图
~~~
同级目录下
~~~
## 4、引入动态编译
流程图中看到了大概的走向，而细节繁多，其中有一个参数的处理，在开发期间可能一个值需要多个函数来处理逻辑后才能拿到，那么对应的编排也是如此，怎么样才能支持参数获取方式的多元化呢，想一想，可以直接是常量，可以是简单脚本，也可以是函数，简单脚本我们可以使用spel或者groovy（也可以当作java使用）都能支持，函数选择jdk自带的动态编译。

## 5、上菜
### a、准备java动态代码模版（此处使用vm模版仅供说明，具体如何生成java代码，看个人怎么实现，最终就是一个字符串）

~~~
package ${packet};
import com.google.common.collect.*;
import org.apache.commons.lang3.*;
import org.apache.commons.lang3.function.*;
import org.apache.commons.lang3.reflect.*;
import org.apache.commons.lang3.math.*;
import org.apache.commons.lang3.concurrent.*;
import org.apache.commons.lang3.tuple.*;
import org.apache.commons.lang3.time.*;
import org.apache.commons.collections4.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;
public class ${name} extends CustomFunction {
    private static final Logger log = LoggerFactory.getLogger(${name}.class);
    @Override
    public DataValue apply(FunctionContext input) throws Exception {
        ${body}
    }
}
~~~
### b、进行编译
#### i、代码步骤
~~~        
//获取java代码模版配置
JavaCustomFunctionTemplate template = this.javaCustomFuncionConfiguration.createTemplate(function.getId(), function.getSourceCode());
//进行变量解析生成java代码
JavaSourceObject javaSourceObject = template.generate();
function.setType(javaSourceObject.getClassName());
//编译
CompiledResult result = this.dynamicCompiler.compile(javaSourceObject);
~~~
#### ii、编译核心代码
~~~        
public class DynamicCompiler {
    public CompiledResult compile(JavaSourceObject... objects) {
        CompiledResult result = new CompiledResult();
        try {
            //获取编译执行官
            JavaCompiler javaCompiler = ToolProvider.getSystemJavaCompiler();
            //收集编译诊断信息
            DiagnosticCollector diagnosticCollector = new DiagnosticCollector();
            //java编译后的信息管理
            JavaClassManager javaClassManager = new JavaClassManager(javaCompiler.getStandardFileManager(diagnosticCollector, null, null));
            List<JavaSourceObject> sourceObjects = Lists.newArrayList(objects);
            StringWriter writer = new StringWriter();
            //编译任务
            JavaCompiler.CompilationTask task = javaCompiler.getTask(writer, javaClassManager, null, null, null, sourceObjects);
            //执行任务（真正去编译的动作）
            Boolean completed = task.call();
            result.setStatus(completed ? CompiledStatus.SUCCEED : CompiledStatus.FAILED);
            result.setDiagnostics(diagnosticCollector);
            if (completed) {
                result.setJavaClassObjects(javaClassManager.getJavaClassObjects());
            } else {
                result.setMessage(writer.getBuffer().toString());
            }
        } catch (Exception ex) {
            log.error("Failed to compile source: " + objects, ex);
            result.setStatus(CompiledStatus.EXCEPTION);
            result.setException(ex);
        }
        return result;
    }
    private static class JavaClassManager extends ForwardingJavaFileManager {
        @Getter
        private List<JavaClassObject> javaClassObjects;
        private JavaClassManager(JavaFileManager fileManager) {
            super(fileManager);
            this.javaClassObjects = new ArrayList<>();
        }
        /**
         * 给编译器提供JavaClassObject，编译器会将编译结果写进去
         */
        @Override
        public JavaFileObject getJavaFileForOutput(Location location, String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException {
            //最重要的信息在这里，拿到编译结果后放到自定义的JavaClassObject里
            JavaClassObject javaClassObject = new JavaClassObject(className, kind);
            this.javaClassObjects.add(javaClassObject);
            return javaClassObject;
        }
    }
}
~~~
#### iii、java代码信息类（继承简单java文件对象即可、另有标准java文件对象StandardJavaFileManager）按需包装
~~~      
public class JavaClassObject extends SimpleJavaFileObject {
    private String type;
    private ByteArrayOutputStream byteArrayOutputStream;
    public JavaClassObject(String className, JavaFileObject.Kind kind) {
        super(URI.create("string:///" + className.replaceAll("\\.", "/") + kind.extension), kind);
        this.type = className;
        this.byteArrayOutputStream = new ByteArrayOutputStream();
    }
    @Override
    public OutputStream openOutputStream() throws IOException {
        return this.byteArrayOutputStream;
    }
    public byte[] getBytes() {
        return this.byteArrayOutputStream.toByteArray();
    }
    public String getType() {
        return type;
    }
}
~~~
#### iiii、class信息返回（继承简单java文件对象即可、另有标准java文件对象StandardJavaFileManager）按需包装
~~~      
public class JavaSourceObject extends SimpleJavaFileObject {
    @Getter
    private String className;
    private String content;
    public JavaSourceObject(String className, String content) {
        super(URI.create("string:///" + className.replaceAll("\\.", "/") + Kind.SOURCE.extension), Kind.SOURCE);
        this.className = className;
        this.content = content;
    }
    @Override
    public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
        return content;
    }
}
~~~
#### v、编译结果包装
~~~ 
public class CompiledResult {
    private CompiledStatus status;
    private String message;
    private DiagnosticCollector diagnostics;
    private List<JavaClassObject> javaClassObjects;
    private Exception exception;
    public JavaClassObject getJavaClassObject(String className) {
        if (null != this.javaClassObjects) {
            for (JavaClassObject javaClassObject : this.javaClassObjects) {
                if (StringUtils.equals(javaClassObject.getType(), className)) {
                    return javaClassObject;
                }
            }
        }
        return null;
    }
}
~~~
## 6、吃菜（运行期load到jvm）
~~~
public OrchestraCustomFunction getFunction(String strId) throws OrchestraException {
    try {
        long id = NumberUtils.toLong(strId);
        //获取编译信息dto
        CustomFunctionDTO customFunction = this.getCustomFunction(id);
        //真正加载到jvm
        Class<OrchestraCustomFunction> aClass = functionClassLoader.loadClass(customFunction);
        //获取构造器
        Constructor<OrchestraCustomFunction> accessibleConstructor = ConstructorUtils.getAccessibleConstructor(aClass, null);
        //返回对象实例
        return accessibleConstructor.newInstance(null);
    } catch (Exception ex) {
        //捕捉异常
    }
}
~~~
~~~
private static class FunctionClassLoader extends SecureClassLoader {
    public FunctionClassLoader(ClassLoader parent) {
        super(parent);
    }
    public Class<OrchestraCustomFunction> loadClass(CustomFunctionDTO customFunction) throws OrchestraException {
        //获取编译代码，就是class字节码信息
        byte[] code = customFunction.getCompiledCode();
        //自定义classloader，重写defineClass方法
        return (Class<OrchestraCustomFunction>) super.defineClass(customFunction.getType(), code, 0, code.length);
    }
}
~~~

