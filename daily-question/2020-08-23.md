# 每日一题

2020年8月23日

#### 题目：
​    RedoLog的二阶段提交：新增或修改一条记录，如果在写完binlog后机器crash了（下图红线处）。
​    此时RedoLog没有这条记录，而binLog里有这条记录，这不就导致数据不一致了吗？
​    

![Mysql数据更新流程](http://javama.oss-cn-hongkong.aliyuncs.com/coca/crash.png)


> 欢迎解答：

如果在红线处crash了，分情况
1. 如果redoLog是完整的，也就是已经有了commit标识：事务commit
2. redoLog不是完整的
    1. binLog是完整的：commit
    2. binLog不是完整的：回滚

    

